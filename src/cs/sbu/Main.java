package cs.sbu;

import cs.sbu.bankManager.BankManager;
import cs.sbu.bankManager.WithdrawSituation;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        run();
    }

    public static void run() {
        Scanner scanner = new Scanner(System.in);
        String command;
        BankManager bankManager = new BankManager("melat");

        do {
            System.out.println("enter command:");
            command = scanner.next();

            String accountNumber;
            double amount;

            switch (command) {
                case "makeAccount":
                    System.out.println("enter name");
                    String name = scanner.next();
                    System.out.println("enter lastname");
                    String lastName = scanner.next();
                    System.out.println("enter ssn");
                    String ssn = scanner.next();
                    System.out.printf("your account number is %s%n",
                            bankManager.makeAccount(name, lastName, ssn));
                    break;
                case "getBalance":
                    System.out.println("enter account number");
                    accountNumber = scanner.next();
                    try {
                        System.out.println(bankManager.getBalance(accountNumber));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "deleteAccount":
                    System.out.println("enter account number");
                    accountNumber = scanner.next();
                    if (bankManager.deleteAccount(accountNumber)) {
                        System.out.println("Account Deleted");
                    } else {
                        System.out.println("Account doesnt exist");
                    }
                    break;
                case "withdraw":
                    accountNumber = scanner.next();
                    amount = scanner.nextInt();
                    WithdrawSituation withdrawSituation = bankManager.withdraw(accountNumber, amount);
                    if (withdrawSituation.equals(WithdrawSituation.ACCEPTED)) {
                        System.out.printf("%f was withdrew from %s%n", amount, accountNumber);
                    } else if (withdrawSituation.equals(WithdrawSituation.DIDNT_EXIST)) {
                        System.out.println("Account doesnt exist");
                    } else {
                        System.out.printf("The balance of account %s was less than %fp%n",
                                accountNumber, amount);
                    }
                    break;
                case "deposit":
                    accountNumber = scanner.next();
                    amount = scanner.nextInt();
                    if (bankManager.addMoney(accountNumber, amount)) {
                        System.out.printf("%f rials was added to %s\n", amount, accountNumber);
                    } else {
                        System.out.println("Account doesnt exist");
                    }
                    break;
            }

        } while (!command.equals("end"));
    }
}
