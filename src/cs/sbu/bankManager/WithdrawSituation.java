package cs.sbu.bankManager;

public enum WithdrawSituation {
    ACCEPTED,
    DIDNT_EXIST,
    NOT_ENOUGH_MONEY
}
