package cs.sbu.bankManager;

import cs.sbu.bankManager.acount.BankAccount;
import cs.sbu.bankManager.acount.Customer;

import java.util.ArrayList;
import java.util.List;

public class BankManager{
    private final List<BankAccount> bankAccounts;
    private final String name;
    private int number;

    public BankManager(String name) {
        this.name = name;
        bankAccounts = new ArrayList<>();
    }

    public String makeAccount(String name, String lastName, String ssn) {
        Customer customer = BankManager.makeCustomer(name, lastName, ssn);
        return this.makeAccount(customer);
    }

    public String makeAccount(Customer customer) {
        BankAccount bankAccount = new BankAccount(customer, String.format("1234%d", number));
        bankAccounts.add(bankAccount);
        return String.format("1234%d", number++);
    }

    public boolean addMoney(String accountNumber, double amount) {
        for (BankAccount ba: bankAccounts) {
            if (ba.getAccountNumber().equals(accountNumber)) {
                ba.deposit(amount);
                return true;
            }
        }

        return false;
    }

    public WithdrawSituation withdraw(String accountNumber, double amount) {
        for (BankAccount ba: bankAccounts) {
            if (ba.getAccountNumber().equals(accountNumber)) {
                if (ba.withdraw(amount)) {
                    return WithdrawSituation.ACCEPTED;
                }
                return WithdrawSituation.NOT_ENOUGH_MONEY;
            }
        }
        return WithdrawSituation.DIDNT_EXIST;
    }

    public boolean deleteAccount(String accountNumber) {
        for (BankAccount ba: bankAccounts) {
            if (ba.getAccountNumber().equals(accountNumber)) {
                bankAccounts.remove(ba);
                return true;
            }
        }
        return false;
    }

    public double getBalance(String accountNumber) throws Exception {
        for (BankAccount ba: bankAccounts) {
            if (ba.getAccountNumber().equals(accountNumber)) {
                return ba.getBalance();
            }
        }
        throw new Exception("account not found");
    }

    public String getName() {
        return this.name;
    }

    public static Customer makeCustomer(String name, String lastName, String ssn) {
        return new Customer(name, lastName, ssn);
    }
}
