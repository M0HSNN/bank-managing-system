package cs.sbu.bankManager.acount;

public class BankAccount {
    private final Customer customer;
    private final String accountNumber;
    private double balance;
    private String password;

    public BankAccount(Customer customer, String accountNumber) {
        this.customer = customer;
        this.accountNumber = accountNumber;
        this.password = "1111";
    }

    public BankAccount(Customer customer, String accountNumber, double balance) {
        this(customer, accountNumber);
        this.balance = balance;
    }

    public BankAccount(Customer customer, String accountNumber, double balance, String password) {
        this(customer, accountNumber, balance);
        this.password = password;
    }

    public Customer getCustomer() {
        return customer;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean withdraw(double amount) {
        if (amount <= this.balance) {
            this.balance -= amount;
            return true;
        } else {
            return false;
        }
    }

    public void deposit(double amount) {
        this.balance += amount;
    }
}
