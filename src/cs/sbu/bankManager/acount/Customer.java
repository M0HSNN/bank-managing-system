package cs.sbu.bankManager.acount;

public class Customer {
    private final String name;
    private final String lastName;
    private final String ssn;

    public Customer(String name, String lastName, String ssn) {
        this.name = name;
        this.lastName = lastName;
        this.ssn = ssn;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSsn() {
        return ssn;
    }
}
